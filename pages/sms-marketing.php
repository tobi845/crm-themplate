<!DOCTYPE html>
<!--
Template Name: Metronic - Responsive Admin Dashboard Template build with Twitter Bootstrap 3.3.6
Version: 4.5.4
Author: JMwebvision Web Development
Website: http://www.jmwebvision.com/
Contact: jmwebvision@gmail.com
-->
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>WeCon | SMS Marketing</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="" name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="http://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="../new-assets/custom.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="../assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/select2/css/select2.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/select2/css/select2-bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.css" rel="stylesheet" type="text/css" />
        <link href="../assets/global/plugins/bootstrap-markdown/css/bootstrap-markdown.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="../assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="../assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->

        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="../assets/pages/css/profile.min.css" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->

        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="../assets/layouts/layout2/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="../assets/layouts/layout2/css/themes/blue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="../assets/layouts/layout2/css/custom.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <!-- <link rel="shortcut icon" href="favicon.ico" /> -->
    </head>
    <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-container-bg-solid">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="index.php">
                        <img class="dashboard-logo" src="../new-assets/images/logo-light.png" alt="logo" /> </a>
                    <div class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE ACTIONS -->
                <!-- DOC: Remove "hide" class to enable the page header actions -->

                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    <!-- BEGIN HEADER SEARCH BOX -->
                    <!-- DOC: Apply "search-form-expanded" right after the "search-form" class to have half expanded search box -->
                    <form class="search-form search-form-expanded" action="page_general_search_3.html" method="GET">
                        <div class="input-group">
                            <input type="text" class="form-control" placeholder="Search..." name="query">
                            <span class="input-group-btn">
                                <a href="javascript:;" class="btn submit">
                                    <i class="icon-magnifier"></i>
                                </a>
                            </span>
                        </div>
                    </form>
                    <!-- END HEADER SEARCH BOX -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default"> 7 </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>
                                            <span class="bold">12 pending</span> notifications</h3>
                                        <a href="page_user_profile_1.html">view all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">just now</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-plus"></i>
                                                        </span> New user registered. </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">3 mins</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-bolt"></i>
                                                        </span> Server #12 overloaded. </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">10 mins</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-warning">
                                                            <i class="fa fa-bell-o"></i>
                                                        </span> Server #2 not responding. </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">14 hrs</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-info">
                                                            <i class="fa fa-bullhorn"></i>
                                                        </span> Application error. </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">2 days</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-bolt"></i>
                                                        </span> Database overloaded 68%. </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">3 days</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-bolt"></i>
                                                        </span> A user IP blocked. </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">4 days</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-warning">
                                                            <i class="fa fa-bell-o"></i>
                                                        </span> Storage Server #4 not responding dfdfdfd. </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">5 days</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-info">
                                                            <i class="fa fa-bullhorn"></i>
                                                        </span> System Error. </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="time">9 days</span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-danger">
                                                            <i class="fa fa-bolt"></i>
                                                        </span> Storage server failed. </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN INBOX DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-extended dropdown-inbox" id="header_inbox_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-envelope-open"></i>
                                    <span class="badge badge-default"> 4 </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>You have
                                            <span class="bold">7 New</span> Messages</h3>
                                        <a href="app_inbox.html">view all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                            <li>
                                                <a href="#">
                                                    <span class="photo">
                                                        <img src="../assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> </span>
                                                    <span class="subject">
                                                        <span class="from"> Lisa Wong </span>
                                                        <span class="time">Just Now </span>
                                                    </span>
                                                    <span class="message"> Vivamus sed auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="photo">
                                                        <img src="../assets/layouts/layout3/img/avatar3.jpg" class="img-circle" alt=""> </span>
                                                    <span class="subject">
                                                        <span class="from"> Richard Doe </span>
                                                        <span class="time">16 mins </span>
                                                    </span>
                                                    <span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="photo">
                                                        <img src="../assets/layouts/layout3/img/avatar1.jpg" class="img-circle" alt=""> </span>
                                                    <span class="subject">
                                                        <span class="from"> Bob Nilson </span>
                                                        <span class="time">2 hrs </span>
                                                    </span>
                                                    <span class="message"> Vivamus sed nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="photo">
                                                        <img src="../assets/layouts/layout3/img/avatar2.jpg" class="img-circle" alt=""> </span>
                                                    <span class="subject">
                                                        <span class="from"> Lisa Wong </span>
                                                        <span class="time">40 mins </span>
                                                    </span>
                                                    <span class="message"> Vivamus sed auctor 40% nibh congue nibh... </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="#">
                                                    <span class="photo">
                                                        <img src="../assets/layouts/layout3/img/avatar3.jpg" class="img-circle" alt=""> </span>
                                                    <span class="subject">
                                                        <span class="from"> Richard Doe </span>
                                                        <span class="time">46 mins </span>
                                                    </span>
                                                    <span class="message"> Vivamus sed congue nibh auctor nibh congue nibh. auctor nibh auctor nibh... </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- END INBOX DROPDOWN -->
                            <!-- BEGIN TODO DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-extended dropdown-tasks" id="header_task_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-calendar"></i>
                                    <span class="badge badge-default"> 3 </span>
                                </a>
                                <ul class="dropdown-menu extended tasks">
                                    <li class="external">
                                        <h3>You have
                                            <span class="bold">12 pending</span> tasks</h3>
                                        <a href="app_todo.html">view all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 275px;" data-handle-color="#637283">
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">New release v1.2 </span>
                                                        <span class="percent">30%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 40%;" class="progress-bar progress-bar-success" aria-valuenow="40" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">40% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Application deployment</span>
                                                        <span class="percent">65%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 65%;" class="progress-bar progress-bar-danger" aria-valuenow="65" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">65% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Mobile app release</span>
                                                        <span class="percent">98%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 98%;" class="progress-bar progress-bar-success" aria-valuenow="98" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">98% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Database migration</span>
                                                        <span class="percent">10%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 10%;" class="progress-bar progress-bar-warning" aria-valuenow="10" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">10% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Web server upgrade</span>
                                                        <span class="percent">58%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 58%;" class="progress-bar progress-bar-info" aria-valuenow="58" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">58% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">Mobile development</span>
                                                        <span class="percent">85%</span>
                                                    </span>
                                                    <span class="progress">
                                                        <span style="width: 85%;" class="progress-bar progress-bar-success" aria-valuenow="85" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">85% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                            <li>
                                                <a href="javascript:;">
                                                    <span class="task">
                                                        <span class="desc">New UI release</span>
                                                        <span class="percent">38%</span>
                                                    </span>
                                                    <span class="progress progress-striped">
                                                        <span style="width: 38%;" class="progress-bar progress-bar-important" aria-valuenow="18" aria-valuemin="0" aria-valuemax="100">
                                                            <span class="sr-only">38% Complete</span>
                                                        </span>
                                                    </span>
                                                </a>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <!-- END TODO DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <img alt="" class="img-circle" src="../new-assets/../new-assets/images/account.png" />
                                    <span class="username username-hide-on-mobile"> Nick </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a href="page_user_profile_1.html">
                                            <i class="icon-user"></i> My Profile </a>
                                    </li>
                                    <li>
                                        <a href="app_calendar.html">
                                            <i class="icon-calendar"></i> My Calendar </a>
                                    </li>
                                    <li>
                                        <a href="app_inbox.html">
                                            <i class="icon-envelope-open"></i> My Inbox
                                            <span class="badge badge-danger"> 3 </span>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="app_todo_2.html">
                                            <i class="icon-rocket"></i> My Tasks
                                            <span class="badge badge-success"> 7 </span>
                                        </a>
                                    </li>
                                    <li class="divider"> </li>
                                    <li>
                                        <a href="page_user_lock_1.html">
                                            <i class="icon-lock"></i> Lock Screen </a>
                                    </li>
                                    <li>
                                        <a href="page_user_login_1.html">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                            <!-- BEGIN QUICK SIDEBAR TOGGLER -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->

                            <!-- END QUICK SIDEBAR TOGGLER -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- END SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    <!-- <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200"> -->
                      <ul class="page-sidebar-menu  page-header-fixed page-sidebar-menu-hover-submenu  page-sidebar-menu-compact" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item">
                            <a href="index.php" class="nav-link nav-toggle">
                                <img class="dashboard-icon" src="../new-assets/images/icons/home.png" alt="profile" />
                                <span class="title">Dashboard</span>
                                <span class="arrow"></span>
                            </a>

                        </li>
                        <li class="nav-item start">
                            <a href="profile.php" class="nav-link nav-toggle">
                                <img class="dashboard-icon" src="../new-assets/images/icons/profile.png" alt="profile" />
                                <span class="title">Profile</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="contacts.php" class="nav-link nav-toggle">
                                <img class="dashboard-icon" src="../new-assets/images/icons/contacts.png" alt="profile" />
                                <span class="title">All Contacts</span>
                                <span class="arrow"></span>
                            </a>
                        </li>

                        <li class="nav-item active open">
                            <a href="sms-campaign.php" class="nav-link nav-toggle">
                                <img class="dashboard-icon" src="../new-assets/images/icons/sms.png" alt="profile" />
                                <span class="title">SMS Marketing</span>
                                <span class="selected"></span>
                                <span class="arrow open"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item  ">
                                    <a href="sms-marketing-list.php" class="nav-link nav-toggle">
                                        <span class="title">List</span>
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="sms-marketing-statistics.php" class="nav-link nav-toggle">
                                        <span class="title">Statistics</span>
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                                <li class="nav-item  ">
                                    <a href="sms-marketing-settings.php" class="nav-link nav-toggle">
                                        <span class="title">Settings</span>
                                        <span class="arrow"></span>
                                    </a>
                                </li>
                            </ul>
                        </li>
                        <li class="nav-item  ">
                            <a href="email-marketing.php" class="nav-link nav-toggle">
                                <img class="dashboard-icon" src="../new-assets/images/icons/email.png" alt="profile" />
                                <span class="title">Email Marketing</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="campaign.php" class="nav-link nav-toggle">
                                <img class="dashboard-icon" src="../new-assets/images/icons/campaign.png" alt="profile" />
                                <span class="title">Campaign</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="schedule.php" class="nav-link nav-toggle">
                                <img class="dashboard-icon" src="../new-assets/images/icons/schedule.png" alt="profile" />
                                <span class="title">Schedule</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="settings.php" class="nav-link nav-toggle">
                                <img class="dashboard-icon" src="../new-assets/images/icons/settings.png" alt="profile" />
                                <span class="title">Settings</span>
                                <span class="arrow"></span>
                            </a>
                        </li>
                        <li class="nav-item  ">
                            <a href="reports.php" class="nav-link nav-toggle">
                                <img class="dashboard-icon" src="../new-assets/images/icons/reports.png" alt="profile" />
                                <span class="title">Reports</span>
                                <span class="arrow"></span>
                            </a>
                        </li>

                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                <!-- BEGIN CONTENT BODY -->
                <div class="page-content">
                    <!-- BEGIN PAGE HEADER-->
                    <!-- BEGIN THEME PANEL -->

                    <!-- END THEME PANEL -->
                    <h3 class="page-title sms-campaign"> SMS Marketing </br>
						                <small>Lorem ipsum dolor sit amet, consectetur adipiscing elit. </small>
                    </h3>

					<!-- ****************************** Start Rows ****************************** -->

					<!-- BEGIN FORM STEPS -->
					<div class="row">
						<div class="col-lg-12">
							<div class="row">
								<div class="col-md-12">
									<div class="portlet light portlet-fit ">
										<div class="portlet-title">
                      <div class="page-bar campaign-bar">
                          <ul class="page-breadcrumb">
                              <li>
                                  <i class="icon-home"></i>
                                  <a href="index.php">Home</a>
                                  <i class="fa fa-angle-right"></i>
                              </li>
                              <li>
                                  <span>SMS Campaign</span>
                              </li>
                          </ul>
                      </div>
										</div>
										<div class="portlet-body">
											<div class="mt-element-step">

												<div class="row step-thin">
													<div class="col-md-3 bg-grey done mt-step-col active" id="campaign-step1">
														<div class="mt-step-number bg-white font-grey"><i class="fa fa-info"></i></div>
														<div class="mt-step-title uppercase font-grey-cascade">Basic Info</div>
														<div class="mt-step-content font-grey-cascade">View all SMS Campaign</div>
													</div>
													<div class="col-md-3 bg-grey mt-step-col" id="campaign-step2">
														<div class="mt-step-number bg-white font-grey"><i class="fa fa-file-text-o"></i></div>
														<div class="mt-step-title uppercase font-grey-cascade">SMS Content</div>
														<div class="mt-step-content font-grey-cascade">Create SMS Content</div>
													</div>
													<div class="col-md-3 bg-grey mt-step-col" id="campaign-step3">
														<div class="mt-step-number bg-white font-grey"><i class="fa fa-user"></i></div>
														<div class="mt-step-title uppercase font-grey-cascade">Recepient</div>
														<div class="mt-step-content font-grey-cascade">Select Contacts</div>
													</div>
													<div class="col-md-3 bg-grey mt-step-col" id="campaign-step4">
														<div class="mt-step-number bg-white font-grey"><i class="fa fa-send-o"></i></div>
														<div class="mt-step-title uppercase font-grey-cascade">Review / Send</div>
														<div class="mt-step-content font-grey-cascade">Review SMS Campaign</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
                    </div>
					<!-- END FORM STEPS -->

					<!-- BEGIN OF DATA TABLE -->
					<div class="row" id="step1">
                        <div class="col-md-12">
                            <!-- BEGIN EXAMPLE TABLE PORTLET-->
                            <div class="portlet light ">
                                <div class="portlet-body">
                                    <div class="table-toolbar">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="btn-group">
							                                     <a id="sample_editable_1_new" class="btn green" data-toggle="modal" href="#basic"> Create SMS Campaign <i class="fa fa-plus"></i> </a>
                                                   <span class="space"> </span>
                                                   <a class="btn red" href="#"> Delete all Campaign <i class="fa fa-trash-o"></i> </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                                        <thead>
                                            <tr>
                                                <th>
                                                    <input type="checkbox" class="group-checkable" data-set="#sample_1 .checkboxes" /> </th>
                                                <th> Campaign name </th>
                                                <th> Description </th>
                                                <th> Status </th>
                                                <th> Date Created </th>
                                                <th class="center edit-delete-buttons"> Actions </th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> Loan Schedules </td>
                                                <td> For the month of January </td>
                                                <td> Draft </td>
                                                <td> December 5, 2015 </td>
                                                <td class="center edit-delete-buttons">
												<a href="javascript:;" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle red btn-sm black"> <i class="fa fa-trash-o"></i> Delete </a>
												 </td>
                                            </tr>

											<tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> Salary Loan </td>
                                                <td> Loan indicating deduction salary </td>
                                                <td> Draft </td>
                                                <td> June 21, 2014 </td>
                                                <td class="center edit-delete-buttons">
												<a href="javascript:;" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle red btn-sm black"> <i class="fa fa-trash-o"></i> Delete </a>
												 </td>
                                            </tr>

											<tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> Financial Loan </td>
                                                <td> Basis on finance estimate </td>
                                                <td> Draft </td>
                                                <td> August 8, 2012 </td>
                                                <td class="center edit-delete-buttons">
												<a href="javascript:;" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle red btn-sm black"> <i class="fa fa-trash-o"></i> Delete </a>
												 </td>
                                            </tr>

											<tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> Collection every month </td>
                                                <td> Send every end of the quarter </td>
                                                <td> Draft </td>
                                                <td> Febuary 26, 2015 </td>
                                                <td class="center edit-delete-buttons">
												<a href="javascript:;" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle red btn-sm black"> <i class="fa fa-trash-o"></i> Delete </a>
												 </td>
                                            </tr>

											<tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> Loan Schedules </td>
                                                <td> For the month of January </td>
                                                <td> Draft </td>
                                                <td> December 5, 2015 </td>
                                                <td class="center edit-delete-buttons">
												<a href="javascript:;" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle red btn-sm black"> <i class="fa fa-trash-o"></i> Delete </a>
												 </td>
                                            </tr>

											<tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> Loan Schedules </td>
                                                <td> For the month of January </td>
                                                <td> Draft </td>
                                                <td> December 5, 2015 </td>
                                                <td class="center edit-delete-buttons">
												<a href="javascript:;" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle red btn-sm black"> <i class="fa fa-trash-o"></i> Delete </a>
												 </td>
                                            </tr>

											<tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> Loan Schedules </td>
                                                <td> For the month of January </td>
                                                <td> Draft </td>
                                                <td> December 5, 2015 </td>
                                                <td class="center edit-delete-buttons">
												<a href="javascript:;" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle red btn-sm black"> <i class="fa fa-trash-o"></i> Delete </a>
												 </td>
                                            </tr>

											<tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> Loan Schedules </td>
                                                <td> For the month of January </td>
                                                <td> Draft </td>
                                                <td> December 5, 2015 </td>
                                                <td class="center edit-delete-buttons">
												<a href="javascript:;" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle red btn-sm black"> <i class="fa fa-trash-o"></i> Delete </a>
												 </td>
                                            </tr>

											<tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> Loan Schedules </td>
                                                <td> For the month of January </td>
                                                <td> Draft </td>
                                                <td> December 5, 2015 </td>
                                                <td class="center edit-delete-buttons">
												<a href="javascript:;" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle red btn-sm black"> <i class="fa fa-trash-o"></i> Delete </a>
												 </td>
                                            </tr>

											<tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> Loan Schedules </td>
                                                <td> For the month of January </td>
                                                <td> Draft </td>
                                                <td> December 5, 2015 </td>
                                                <td class="center edit-delete-buttons">
												<a href="javascript:;" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle red btn-sm black"> <i class="fa fa-trash-o"></i> Delete </a>
												 </td>
                                            </tr>

											<tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> Loan Schedules </td>
                                                <td> For the month of January </td>
                                                <td> Draft </td>
                                                <td> December 5, 2015 </td>
                                                <td class="center edit-delete-buttons">
												<a href="javascript:;" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle red btn-sm black"> <i class="fa fa-trash-o"></i> Delete </a>
												 </td>
                                            </tr>

											<tr class="odd gradeX">
                                                <td>
                                                    <input type="checkbox" class="checkboxes" value="1" /> </td>
                                                <td> Loan Schedules </td>
                                                <td> For the month of January </td>
                                                <td> Draft </td>
                                                <td> December 5, 2015 </td>
                                                <td class="center edit-delete-buttons">
												<a href="javascript:;" class="btn btn-outline btn-circle btn-sm blue"> <i class="fa fa-edit"></i> Edit </a>
												<a href="javascript:;" class="btn btn-outline btn-circle red btn-sm black"> <i class="fa fa-trash-o"></i> Delete </a>
												 </td>
                                            </tr>

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Step 2-->

                    <div class="row" id="step2">
                      <div class="col-md-12">
                        <div class="col-md-12 portlet light campaign-content">
                            <div class="form-group">
                                <div class="col-md-8">
                                    <textarea name="markdown" data-provide="markdown" rows="10" data-error-container="#editor_error"></textarea>
                                    <div id="editor_error"> </div>
                                    <p><strong> Edit the content of your SMS  Campaign here. </strong> <br>
                                      Content is limited to 300 characters only. Please make sure your content has some reference to you and your company.
                                    </p>
                                </div>
                                <div class="col-md-4">
                                  <div class="preview">
                                    <div class="preview-message">
                                        <div class="message you">
                                          <p>Content is limited to 300 characters only. Please make sure your content has some reference to you and your company. </p>
                                        </div>
                                    </div>
                                  </div>
                                </div>

                            </div>
                            <div class="col-md-12 col-xs-12">
                              <button type="button" class="btn btn-danger"><i class="fa fa-close"></i> Cancel</button>
                              <button type="submit" class="btn btn-info"><i class="fa fa-save"></i> Save and Exit</button>
                              <button type="submit" class="btn btn-success"><i class="fa fa-check"></i> Save and Continue</button>
                            </div>
                        </div>
                      </div>


                    </div>

					<!-- ****************************** End Rows ******************************** -->

				</div>
                <!-- END CONTENT BODY -->
            </div>
            <!-- END CONTENT -->

			<!-- BEGIN MODALS -->
			<div class="modal fade" id="basic" tabindex="-1" role="basic" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
							<h4 class="modal-title">Create New SMS Campaign</h4>
						</div>
						<div class="modal-body">
							<div class="form-group">
								<input type="text" class="campaign-name form-control" placeholder="Enter SMS Campaign name...">
								<input type="text" class="campaign-name form-control" placeholder="Enter SMS Campaign description...">

                            </div>
						</div>
						<div class="modal-footer">
							<a href="javascript:;" class="btn dark btn-outline" data-dismiss="modal" aria-hidden="true"> Cancel</a>
							<a href="javascript:;" class="btn green" id="blockui_sample_2_4"> Save and Continue</a>
						</div>
					</div>
					<!-- /.modal-content -->
				</div>
				<!-- /.modal-dialog -->
			</div>
			<!-- END MODALS -->

            <!-- BEGIN QUICK SIDEBAR -->

            <!-- END QUICK SIDEBAR -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
          <div class="page-footer-inner">
            2016 © WeCon. All Rights Reserved.
          </div>
            <div class="scroll-to-top">
                <i class="icon-arrow-up"></i>
            </div>
        </div>
        <!-- END FOOTER -->
        <!--[if lt IE 9]>
<script src="../assets/global/plugins/respond.min.js"></script>
<script src="../assets/global/plugins/excanvas.min.js"></script>
<![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="../assets/global/plugins/jquery.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/js.cookie.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-hover-dropdown/bootstrap-hover-dropdown.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
		    <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="../assets/global/scripts/datatable.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/select2/js/select2.full.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/jquery-validation/js/additional-methods.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/wysihtml5-0.3.0.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-wysihtml5/bootstrap-wysihtml5.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/ckeditor/ckeditor.js" type="text/javascript"></script>
        <script src="../assets/global/plugins/bootstrap-markdown/lib/markdown.js" type="text/javascript"></script>
        <script src="./../assets/global/plugins/bootstrap-markdown/js/bootstrap-markdown.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="../assets/global/scripts/app.min.js" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
		    <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="../assets/pages/scripts/table-datatables-managed.min.js" type="text/javascript"></script>
	      <script src="../assets/pages/scripts/ui-blockui.min.js" type="text/javascript"></script>
        <script src="../new-assets/script.js" type="text/javascript"></script>
        <script src="../assets/pages/scripts/form-validation.min.js" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="../assets/layouts/layout2/scripts/layout.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/layout2/scripts/demo.min.js" type="text/javascript"></script>
        <script src="../assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
    </body>

</html>
