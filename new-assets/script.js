$("#step2").hide();

$( "#blockui_sample_2_4" ).click(function() {

  window.setTimeout(function () {
    $('#basic').modal('hide');
  }, 500);

  $("#step1").hide(500);

  $("#campaign-step1").removeClass("done");
  $("#campaign-step1").removeClass("active");

  $("#step2").show(500);
  $("#campaign-step2").addClass("done");
  $("#campaign-step2").addClass("active");


});
